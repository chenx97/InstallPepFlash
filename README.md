# InstallPepFlash

## Farewell Notice

Goodbye Flash.

## Original

now compatible with old Debian installation... in a limited way. the latest directory preferred by adobe-flashplayer is available when the system has no such an installation. more detection needed, such as the detection of whether chromium executable is a script.

### How to install:

 * Download PPAPI flash installer in .tar.gz format from Adobe
 * Put the PPAPI installer archive in the same folder with the script
 * Run the code as root
 * Enjoy (if you can really enjoy) the flash-equipped experience.
